﻿using System;

namespace TemperatureConversion.Exceptions
{
    public class UnsupportedUnitToConvertException : Exception 
    {
    }
}
