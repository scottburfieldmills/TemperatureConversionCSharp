﻿using System;

namespace TemperatureConversion.Units
{
    public abstract class AbstractConverter
    {
        public abstract decimal Convert(char unitToConvertFrom, decimal amount);

        /// <summary>
        /// Round a decimal value to 2 decimal places.
        /// </summary>
        /// <param name="amount">Decimal amount to be rounded</param>
        /// <returns>Decimal value rounded to 2 decimal places</returns>
        public decimal Round(decimal amount)
        {
            return decimal.Round(amount, 2, MidpointRounding.AwayFromZero);
        }
    }
}
