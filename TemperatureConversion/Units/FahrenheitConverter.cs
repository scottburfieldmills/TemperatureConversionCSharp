﻿using System;
using TemperatureConversion.Exceptions;

namespace TemperatureConversion.Units 
{
    public class FahrenheitConverter : AbstractConverter
    {
        public override decimal Convert(char unitToConvertFrom, decimal amount)
        {
            switch (unitToConvertFrom)
            {
                case 'c':
                    return FromCelsius(amount);
                default:
                    throw new UnsupportedUnitToConvertException();
            }
        }

        /// <summary>
        /// Convert a temperature from Celsius to Fahrenheit. 
        /// </summary>
        /// <param name="amount">Temperature to be converted into Fahrenheit</param>
        /// <returns>Fahrenheit temperature rounded to 2 decimal places</returns>
        public decimal FromCelsius(decimal amount)
        {
            Console.WriteLine("{0} * 9 / 5 + 32", amount);
            decimal convertedAmount = amount*9/5 + 32;  

            return Round(convertedAmount); 
        }
    }
}
