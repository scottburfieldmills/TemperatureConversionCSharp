﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemperatureConversion.Exceptions;

namespace TemperatureConversion.Units
{
    public class CelsiusConverter : AbstractConverter
    {
        public override decimal Convert(char unitToConvertFrom, decimal amount)
        {
            Console.WriteLine("Celsius Converter " + unitToConvertFrom);

            switch (unitToConvertFrom)
            {
                case 'f':
                    return FromFahrenheit(amount);
                default:
                    throw new UnsupportedUnitToConvertException();
            }
        }

        /// <summary>
        /// Convert a temperature from Fahrenheit to Celsius.
        /// </summary>
        /// <param name="amount">Temperature to be converted into Celsius</param>
        /// <returns>Celsius tempeature rounded to 2 decimal places</returns>
        public decimal FromFahrenheit(decimal amount)
        {
            return (amount - 32) / (decimal) 1.8;
        }
    }
}
