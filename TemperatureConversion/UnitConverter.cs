﻿using TemperatureConversion.Exceptions;
using TemperatureConversion.Units;

namespace TemperatureConversion
{
    public class UnitConverter
    {
        public decimal Convert(decimal amountToConvert, char unitToConvertFrom, char unitToConvertTo)
        {
            var unitConverter = GetConverter(unitToConvertTo);

            return unitConverter.Convert(unitToConvertFrom, amountToConvert); 
        }

        /// <summary>
        /// Get the Converter class based on the type of unit the value should be converted from 
        /// </summary>
        /// <param name="unitOfMeasurement" />
        /// <returns>Instance of the class that handles converting the temperature into the desired unit of measurement</returns>
        protected AbstractConverter GetConverter(char unitOfMeasurement)
        {
            switch (unitOfMeasurement)
            {
                case 'c':
                    return new CelsiusConverter();
                case 'f':
                    return new FahrenheitConverter();
                default:
                    throw new UnsupportedUnitToConvertException();
            }
        }
    }
}
