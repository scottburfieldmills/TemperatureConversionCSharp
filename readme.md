﻿# Temperature Conversion

Simple project for converting between temperature units such as Celsius and Fahrenheit. The purpose of this is just to 
get me familiar with C# again and to try out Unit Testing in C#.

This can be done using the UnitConverter class

```
UnitConverter converter = new UnitConverter();

// Converts from Fahrenheit to Celsius 
converter.Convert((decimal)40, 'f', 'c');

// Converts from Celsius to Fahrenheit 
converter.Convert((decimal)80, 'c', 'f');  
```

Alternatively the classes can be used directly:

```
FahrenheitConverter converter = new FahrenheitConverter();

// Passing the first letter of the unit as the type 
converter.Convert('c', 40);

// Directly calling the method in the class
converter.FromCelsius(40);
```