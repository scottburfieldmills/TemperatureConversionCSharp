﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TemperatureConversion.Exceptions;
using TemperatureConversion.Units;

namespace TemperatureConversionTests.Units
{
    [TestClass]
    public class CelsiusConverterTests
    {
        private CelsiusConverter _converter;

        [TestInitialize]
        public void SetUp()
        {
            _converter = new CelsiusConverter();
        }

        [TestMethod]
        public void ConvertsFahrenheitToCelsiusTest()
        {
            Assert.AreEqual(100, _converter.Convert('f', 212));

            Assert.AreEqual((decimal)4.45, _converter.Convert('f', (decimal)40.01));
        }

        [TestMethod]
        [ExpectedException(typeof(UnsupportedUnitToConvertException), "An invalid unit of measurement to convert from was allowed")]
        public void ConvertingFromAnUnknownUnitFails()
        {
            _converter.Convert('z', (decimal) 1.23);
        }
    }
}
