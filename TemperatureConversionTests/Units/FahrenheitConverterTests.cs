﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TemperatureConversion.Exceptions;
using TemperatureConversion.Units;

namespace TemperatureConversionTests.Units
{
    [TestClass]
    public class FahrenheitConverterTests
    {
        private FahrenheitConverter _fahrenheitConverter;

        [TestInitialize]
        public void SetUp()
        {
            _fahrenheitConverter = new FahrenheitConverter();
        }

        [TestMethod()]
        public void ConvertsToFahrenheitFromCelsius()
        {
            Assert.AreEqual((decimal)227.3, _fahrenheitConverter.Convert('c', (decimal)108.5));
            Assert.AreEqual(212, _fahrenheitConverter.Convert('c', 100));
        }

        [TestMethod]
        [ExpectedException(typeof(UnsupportedUnitToConvertException), "An invalid unit of measurement to convert from was allowed")]
        public void ConvertingFromAnUnknownUnitFails()
        {
            _fahrenheitConverter.Convert('z', (decimal)1.23);
        }

    }
}
