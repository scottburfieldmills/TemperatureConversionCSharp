﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TemperatureConversion;
using TemperatureConversion.Exceptions;

namespace TemperatureConversionTests
{
    [TestClass()]
    public class UnitConverterTests
    {
        private UnitConverter _unitConverter;

        [TestInitialize]
        public void SetUp()
        {
            _unitConverter = new UnitConverter();
        }

        [TestMethod()]
        [ExpectedException(typeof(UnsupportedUnitToConvertException), "An invalid unit of measurement to convert from was allowed")]
        public void ConvertingInvalidUnitCausesExceptionTest()
        {
            _unitConverter.Convert((decimal) 1.23, 'z', 'x');
        }
    }
}